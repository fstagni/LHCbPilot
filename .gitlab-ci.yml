###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# This .gitlab-ci.yml is for pylint, pytest, pep8 compliance,
# and for checking that the copyright is added to each file

# It runs on cc7 and on slc6

stages:
   # Run all the tests
  - test
  - copyright

# Common configuration between pytest and pylint
.test_setup:
  stage: test
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  before_script:
    - git clone https://github.com/DIRACGrid/Pilot.git
    - cd Pilot
    # The following should checkout devel or master depending on the current branch
    - git checkout $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    #- git checkout master
    - cd ..
    - pip install 'setuptools<45'
    - pip install --upgrade 'pip<21'
    - pip install virtualenv
    - virtualenv DIRAC_VENV
    - source DIRAC_VENV/bin/activate
    - pip install 'setuptools<45'
    - pip install --upgrade 'pip<21'
    - pip list
    - pip install -r Pilot/requirements.txt
    - pip install LbEnv LbPlatformUtils
  only: 
    - branches

# Common configuration between pytest and pylint
.test_setup_py3:
  stage: test
  image: registry.cern.ch/docker.io/condaforge/miniforge3:latest
  before_script:
    - eval "$(python -m conda shell.bash hook)"
    - conda install --yes mamba git
    - git clone https://github.com/DIRACGrid/Pilot.git
    - cd Pilot
    # The following should checkout devel or master depending on the current branch
    - git checkout $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - cd ..
    - mamba env create --file Pilot/environment-py3.yml --name test-env
    - conda activate test-env
  only:
    - branches


# This runs pylint on cc7/py2 and fails if there are errors
run_pylint_cc7:
  extends: .test_setup
  script:
    - cd LHCbPilot
    - 'find . -name "*.py" -exec pylint -E --rcfile=$CI_PROJECT_DIR/.pylintrc --msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" {} +'

# This runs pylint on py3 and fails if there are errors
run_pylint_py3:
  extends: .test_setup_py3
  script:
    - cd LHCbPilot
    - 'find . -name "*.py" -exec pylint -E --rcfile=$CI_PROJECT_DIR/.pylintrc --msg-template="{path}:{line}: [{msg_id}({symbol}), {obj}] {msg}" {} +'

# # This would run pytest on cc7, but right now there are no tests
# run_pytest_cc7:
#   extends: .test_setup
#   image:  gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
#   script:
#     - cd LHCbPilot
#     - pytest

# This runs pycodestyle on cc7/py2
run_pycodestyle_cc7:
  extends: .test_setup
  script:
    - lhcbPilotBranch=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - echo $lhcbPilotBranch
    - git remote add GL https://gitlab.cern.ch/lhcb-dirac/LHCbPilot.git
    - git fetch --no-tags GL
    - git branch -avv
    #- git diff -U0 GL/${lhcbPilotBranch} | pycodestyle --diff
    - git diff -U0 GL/master | pycodestyle --diff

# This runs pycodestyle on py3
run_pycodestyle_py3:
  extends: .test_setup_py3
  script:
    - lhcbPilotBranch=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - echo $lhcbPilotBranch
    - git remote add GL https://gitlab.cern.ch/lhcb-dirac/LHCbPilot.git
    - git fetch --no-tags GL
    - git branch -avv
    #- git diff -U0 GL/${lhcbPilotBranch} | pycodestyle --diff
    - git diff -U0 GL/master | pycodestyle --diff


# This ensures the license text is present
# Based on: https://gitlab.cern.ch/lhcb/LHCb/blob/master/.gitlab-ci.yml#L14-18
check-copyright:
  image: gitlab-registry.cern.ch/ci-tools/ci-worker:cc7
  stage: copyright
  before_script:
    - git remote add upstream https://gitlab.cern.ch/lhcb-dirac/LHCbPilot.git
    - git fetch --all
    - curl -o lb-check-copyright "https://gitlab.cern.ch/lhcb-core/LbDevTools/raw/master/LbDevTools/SourceTools.py?inline=false"
  script:
    - python lb-check-copyright upstream/devel --exclude '^tests/.*\.json$'
